package restassured.testswithspecasseparateclass;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import restassured.testswithspecasseparateclass.config.SmartHomeConfig;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

public class SmartHomeTests extends SmartHomeConfig {

    private static final String DIVICES_ACTIONS_PATH = "/devices/actions";
    private static final String DIVICES_PATH = "devices/";
    private static final String USER_INFO_PATH = "user/info";

    String orderRequest1 = """
            {
            	"devices": [{
            		"id":"123",
            		"actions": [{
            			"type": "devices.capabilities.on_off",
            			"state": {
                            "instance": "on",
                            "value": true
                        }
            		}]
            	}]
            }
            """;

    @Test
    public void getRequest() {
        given()
                .when()
                .get(DIVICES_PATH + "123")
                .then();
    }

    @Test
    public void postRequest() {
        given()
                .body(orderRequest1)
                .when()
                .post(DIVICES_ACTIONS_PATH)
                .then();
    }

    @Test
    public void getRequest1() {
        given()
                .when()
                .get(DIVICES_PATH + "123")
                .then()
                .body("room", equalTo("456"));
    }

    @Test
    public void getRequest_extractResponse() {
        Response response =
                given()
                        .when()
                        .get(DIVICES_PATH + "123")
                        .then()
                        .contentType(ContentType.JSON)
                        .extract().response();
        String jsonReSponseAsString = response.asString();
        Assertions.assertTrue(jsonReSponseAsString.contains("\"name\":\"Выключатель\""));
    }

    @Test
    public void getRequest_checkAllRoomsNames() {
        List<String> expectedResult = Arrays.asList("Гостиная", "Спальня", "Коридор", "Кухня", "Ванная", "Кладовка", "Туалет");
        Response response =
                get(USER_INFO_PATH)
                        .then()
                        .extract().response();
        List<String> roomNames = response.path("rooms.name");
        for (String roomName : roomNames) {
            Assertions.assertTrue(expectedResult.contains(roomName));
        }
    }

    @Test
    public void getRequest_assertOnResponseTime() {
        get(DIVICES_PATH + "123")
                .then().time(lessThan(2000L));
    }

    @Test
    public void getRequest_extractMapOfRoomDataWithFind() {
        Response response = get(USER_INFO_PATH);
        Map<String, ?> allDataForDevice = response.path("rooms.find { it.id == '123' }");
        for (Map.Entry<String, ?> entry : allDataForDevice.entrySet()) {
            if (entry.getKey().equals("name")) {
                Assertions.assertEquals("Кладовка", entry.getValue());
            }
        }
    }

    @Test
    public void getRequest_extractSingleValueWithFind() {
        Response response = get(USER_INFO_PATH);
        String roomName = response.path("rooms.find { it.id == '123' }.name");
        Assertions.assertEquals("Гостиная", roomName);
    }
}