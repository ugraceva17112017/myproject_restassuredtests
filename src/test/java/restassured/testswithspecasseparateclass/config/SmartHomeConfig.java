package restassured.testswithspecasseparateclass.config;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.jupiter.api.BeforeEach;

import static org.hamcrest.Matchers.lessThan;

public abstract class SmartHomeConfig {
    private static final String BEARER_TOKEN = "Bearer 123";

    @BeforeEach
    public void setup() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api.iot.yandex.net/")
                .setBasePath("v1.0/")
                .setContentType("application/json")
                .addHeader("Authorization", BEARER_TOKEN)
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .build();

        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectResponseTime(lessThan(3000L))
                .build();
    }
}



