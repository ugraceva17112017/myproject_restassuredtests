package restassured.testswithspec;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;

public class SmartHomeTests {

    RequestSpecification requestSpecification;
    ResponseSpecification responseSpecification;

    private static final String BEARER_TOKEN = "Bearer 123";
    String orderReq = """
            {
            	"devices": [{
            		"id":"123",
            		"actions": [{
            			"type": "devices.capabilities.on_off",
            			"state": {
                            "instance": "on",
                            "value": false
                        }
            		}]
            	}]
            }
                    """;
    String orderReq1 = """
            {
                "actions": [
                    {
                        "type": "devices.capabilities.on_off",
                        "state": {
                            "instance": "on",
                            "value": true
                        }
                    }
                ]
            }
            """;


    @BeforeEach
    public void init() {
        requestSpecification = given().baseUri("https://api.iot.yandex.net/v1.0/")
                .header("Authorization", BEARER_TOKEN)
                .filter(new RequestLoggingFilter())
                .filter(new ResponseLoggingFilter());
        responseSpecification = expect().
                statusCode(200).
                contentType(ContentType.JSON);
    }

    @Test
    public void getRequest2Spec() {
        given().spec(requestSpecification).
                expect().spec(responseSpecification).
                when().get("devices/123").
                then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .assertThat()
                .body("name", Matchers.is("Настольная лампа"));
    }

    @Test
    public void postRequest1Spec() {
        given()
                .spec(requestSpecification)
                .body(orderReq)
                .expect()
                .spec(responseSpecification)
                .when()
                .post("devices/actions")
                .then()
                .assertThat();
    }

    @Test
    public void postRequest2Spec() {
        RestAssured
                .given()
                .spec(requestSpecification)
                .body(orderReq1)
                .expect()
                .spec(responseSpecification)
                .when()
                .post("groups/123/actions")
                .then()
                .assertThat();
    }

}



















