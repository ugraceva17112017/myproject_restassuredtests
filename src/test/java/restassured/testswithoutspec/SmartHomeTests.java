package restassured.testswithoutspec;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

public class SmartHomeTests {
    private static final String BEARER_TOKEN = "Bearer 123";
    private static final String BASE_URL = "https://api.iot.yandex.net/v1.0";
    String orderReq = """
            {
            	"devices": [{
            		"id":"123",
            		"actions": [{
            			"type": "devices.capabilities.on_off",
            			"state": {
                            "instance": "on",
                            "value": false
                        }
            		}]
            	}]
            }
                    """;

    @Test
    public void getRequest1() {
        RestAssured
                .given()
                .log().all()
                .when()
                .contentType(ContentType.JSON)
                .header("Authorization", BEARER_TOKEN)
                .body(orderReq)
                .post(BASE_URL + "/user/info").
                then()
                .log().all()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    public void getRequest2() {
        RestAssured
                .given()
                .log().all()
                .when()
                .header("Authorization", BEARER_TOKEN)
                .get(BASE_URL + "/devices/123")
                .then()
                .log().all()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    public void postRequest1() {
        RestAssured
                .given()
                .log().all()
                .when()
                .contentType(ContentType.JSON)
                .header("Authorization", BEARER_TOKEN)
                .body(orderReq)
                .post(BASE_URL + "/devices/actions")
                .then()
                .log().all()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    public void getRequest3() {
        RestAssured
                .given()
                .log().all()
                .when().header("Authorization", BEARER_TOKEN)
                .get(BASE_URL + "/groups/123")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }
}
